from b2c2.settings import *

DEBUG = True
DEBUG_TOOLBAR = False
NGROK = True

if DEBUG_TOOLBAR:
    THIRD_PARTY_APPS += [
        "debug_toolbar",
    ]
    MIDDLEWARE = ["debug_toolbar.middleware.DebugToolbarMiddleware"] + MIDDLEWARE

ALLOWED_HOSTS = [
    "localhost",
    "127.0.0.1",
    "63e45452584d.ngrok.io",
    "https://63e45452584d.ngrok.io",
]

if not NGROK:
    CACHES = {
        "default": {
            "BACKEND": "django.core.cache.backends.dummy.DummyCache",
        }
    }

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": os.path.join(BASE_DIR, "db.sqlite3"),
        "TEST": {
            "NAME": "test_database",
        },
    }
}

SECURE_SSL_REDIRECT = False
SESSION_COOKIE_SECURE = False
SECURE_HSTS_SECONDS = None

# Allauth
ACCOUNT_EMAIL_VERIFICATION = "optional"

EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"

print(f"Debug: {DEBUG}")
