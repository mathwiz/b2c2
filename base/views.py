from django.shortcuts import render, redirect
from django.urls import reverse_lazy


def homepage(request):
    return redirect(reverse_lazy('trade_account:dashboard'))


def handler404_view(request, *args, **kwargs):
    response = render(request, "404.html", status=404)
    return response


def handler500_view(request, *args, **kwargs):
    response = render(request, "500.html", status=500)
    return response
