from django.conf.urls import url
from django.contrib.staticfiles.storage import staticfiles_storage
from django.views.generic import TemplateView, RedirectView

from base.views import handler500_view, handler404_view, homepage
from b2c2 import settings

app_name = 'base'

urlpatterns = [
    url(r'^$', homepage, name='homepage'),
    url("favicon.ico", RedirectView.as_view(url=staticfiles_storage.url("img/b2c2_favicon_v1.webp"))),
]

handler500 = handler500_view
handler404 = handler404_view

if settings.DEBUG:
    urlpatterns += [
        url(r'^500/$', handler500),
        url(r'^404/$', handler404),
    ]
