# B2C2

![Order Engine Image](docs/images/b2c2_frontend.png)

## 0. Setup
* Python 3.8
* Django 3.2 implementation with local SQLite DB
* `pip install -r requirements.txt` after setting up a virtual environment.
* Add your `B2C2_UAT_TOKEN` to the `.env` file and run the server
* You can optionally navigate to https://63e45452584d.ngrok.io if you would like 
to hit my local server (if it's running at the time).

## 1. Overall Approach

Design Patterns focused on:
1. Code Reusability
2. Behavioral Command Design Pattern where you can have multiple apis, and interface, producers and consumers
and these interact with each other with loose coupling.

This can also be seen how the apps and libraries were divided. For example, `apis` can define
more than one API (In this case there's obviously only `b2c2_api`).

### 1a. Implementation Details

```python
urlpatterns = [
    url(r'^balance$', BalanceHandler.as_view(), name='get_balance'),
    url(r'^rfq$', RfqHandler.as_view(), name='rfq_handler'),
    url(r'^order$', OrderHandler.as_view(), name='order_handler'),
    url(r'^open_positions$', OpenPositionsHandler.as_view(), name='open_positions'),
]
```

These endpoints are interacted with via a vanilla JQuery API in the frontend through the user interface.

```javascript
$.ajax({
    type: "POST",
    url: "api/order",
    data: formData,
    dataType: "json",
    mode: 'same-origin',
    headers: {"X-CSRFToken": $("input[name=csrfmiddlewaretoken]")[0].value},
    encode: true,
    success: handleOrderSuccess,
    error: handleFormError,
});
``` 

Flow of Services, REST, and Output:

```python
service = B2C2BalanceService()  # Balance processing
response = service.get_balance()  # Producer
service.process_response()  # Calls B2C2BalanceOutput to verify and process data for Consumption
if service.is_valid:
    output: B2C2BalanceOutput = service.output
```

### 1b. Technical Details

REST API uses the following classes:  

1. Abstract `RestBackend` which defines the base functionality for REST and is generic enough to be API independent
    * B2C2 Specific:
        * `B2C2RestBackend` subclasses this.
    * Other API Backends would also subclass it. 
2. Services subclass `B2C2BaseService`, which in turn uses `RestBackend` for requests.
    * B2C2 Specific:
        * `B2C2BalanceService`
        * `B2C2InstrumentsService`
        * `B2C2RfqService`
        * `B2C2OrderService`
    * Services are treated as **Producers**
3. Outputs (e.g. `b2c2_datastructures.py`) are treated as **Consumers**
    * They take a REST output, pass it through **Serializers**, and then return the **Output** to be consumed elsewehere
    * This extra **Output** adds for post-processing functionality and ease-of-use.
    * B2C2 Specific:
        * `B2C2InstrumentsOutput` uses `B2C2InstrumentSerializer`
        * `B2C2RfqOutput` uses `B2C2RfqSerializer`
        * `B2C2OrderOutput` uses `B2C2OrderSerializer`, `B2C2TradeSerializer`
        * `B2C2BalanceOutput`


### 1c. Error Handling

Error handling is catching specific exceptions and returning the error in the API.
Ideally the returns are standardized but I didn't have much time to clean the Error part up.

The `ErrorHandler` aims to standardize client facing exception handling.
Backend facing exception handling is implemented in different places depending on the situation, e.g.

* In the `RestBackend`, specific exceptions are caught
* Native Serializers do a good job at giving errors, which are then checked and returned in the API.
* Improvements: 
    * The `ErrorHandler` mentioned above would take these exceptions and standardize the output
    as opposed to the mess which is currently in some of the endpoints (e.g. `data['API Error']`)

## 3. Database Models

Relevant Database models follow the same approach as the API.

DB Entries are saved according to the workflow:
1. RFQs are saved when the user requests them and the response is valid.
2. If the RFQ is valid, it's presented to the user. Once accepted, the order is
sent and ANY order which is received is saved (whether it's valid or not) along with the trades.

![Order Engine Image](docs/images/b2c2_db_order_engine.png)

## 4. Testing

While testing is not 100% complete, focus was on:
1. Backend API Integration Testing
2. Backend Functional Testing

As discussed in (2), the separation of concerns allowed for easier testing of specific components and
I hope this shows what strategy I'm going for even though I didn't have time to do all tests.
All tests pass.

## 5. Improvements

1. Error Handling
2. Async
    * Given the separation of concerns, we can implement async around the `make_request` using `uvicorn` and `django`.
    * This would obviously improve performance and handle higher loads.
3. ~~Checking for Balance~~ - DONE
    * ~~There is no logic for checking balance with the engine. It can be added.~~
4. ~~Handling more specific errors, e.g. outdated RFQ.~~ - PARTIALLY DONE
5. JS Cleanup
6. Stream implementation
7. Throttling
8. Requests surveillance
10. ~~The B2C2_UAT_TOKEN is Profile specific but in this case I left it for the app.~~ - PARTIALLY DONE
11. I can go on...