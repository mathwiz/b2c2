from django import forms
from django_utils.choices import Choice

from apis.b2c2_api.b2c2_constants import B2C2BuySellSide


class RfqForm(forms.Form):
    instruments = forms.CharField(max_length=64,
                                  widget=forms.Select(choices=[], attrs={'required': 'True'}),
                                  required=True,
                                  min_length=3)
    buy_sell = forms.CharField(widget=forms.Select(choices=[(side, side) for side in B2C2BuySellSide.options()],
                                                   attrs={'required': 'True'}),
                               max_length=64,
                               label="Side",
                               required=True,
                               min_length=3)
    quantity = forms.DecimalField(max_digits=6,
                                  widget=forms.NumberInput(attrs={"required": "required", "step": "0.0001"}),
                                  help_text="Max up to 6 digits", required=True, )
    session = forms.CharField(widget=forms.HiddenInput)

    def __init__(self, *args, **kwargs):
        instruments_choices = kwargs.pop("instruments_choices", None)
        super().__init__(*args, **kwargs)
        if instruments_choices:
            choices = [(inst_name, Choice(inst_name, inst_name)) for inst_name in instruments_choices]
            self.fields["instruments"].choices = choices
            self.fields["instruments"].widget.choices = choices
