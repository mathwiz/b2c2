from django.conf.urls import url
from trade_account.views import DashboardHandler

app_name = 'trade_account'

urlpatterns = [
    url(r'^dashboard$', DashboardHandler.as_view(), name='dashboard'),
]
