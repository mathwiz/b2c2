from django.contrib import messages
from django.shortcuts import render
from django.urls import reverse_lazy
from rest_framework.views import APIView
from django.contrib.auth.mixins import LoginRequiredMixin
from apis.b2c2_api.b2c2_services import B2C2InstrumentsService
from order_engine.views import get_processed_balance_service
from trade_account.forms import RfqForm


class DashboardHandler(LoginRequiredMixin, APIView):
    login_url = reverse_lazy("account_login")

    def get(self, request):
        profile = request.user.profile
        instruments_service = B2C2InstrumentsService(profile.b2c2_auth_token)
        instruments_service.process_response(instruments_service.get_instruments())
        initial_data = {"session": self.request.session.session_key}
        if instruments_service.is_valid:
            form = RfqForm(initial=initial_data,
                           instruments_choices=instruments_service.output.all_instruments)
            template_context = {"form": form}
            _ = get_processed_balance_service(profile.b2c2_auth_token, template_context)
            return render(request, "dashboard.html", template_context)
        else:
            messages.error(request, instruments_service.rest_backend.exception_message, extra_tags="danger")
            return render(request, "dashboard.html", {"form": RfqForm(initial=initial_data), "balance_data": {}})
