from django.contrib import admin
from trade_account.models import Profile


class ProfileAdmin(admin.ModelAdmin):
    list_display = ['user',
                    'balances',
                    'b2c2_auth_token',
                    ]
    list_display_links = ['user']


admin.site.register(Profile, ProfileAdmin)
