from allauth.account.signals import user_signed_up
from annoying.fields import AutoOneToOneField
from django.db import models

from django.conf import settings
from django.dispatch import receiver

from utils.logging_util import log


class Profile(models.Model):
    user = AutoOneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='profile')
    balances = models.JSONField(null=True, blank=True)
    b2c2_auth_token = models.CharField(max_length=64,
                                       # This is only here for the test
                                       default=settings.API_ENVIRONMENTS["B2C2"].get('authorization', ''))


@receiver(user_signed_up)
def user_signed_up_signal(request, user, **kwargs):
    Profile.objects.create(user=user)
    log.info("Profile created")
