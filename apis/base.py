from typing import Optional
from requests.models import Response
from rest_framework import status
from urllib.parse import urljoin

from rest_framework.status import is_success

from apis.exceptions import GenericAPIException
from b2c2 import settings
from utils.logging_util import log
from abc import ABC
import requests


class RestBackend(ABC):
    """
    Abstract class helper for general REST backends.
    Defines the base backend functionality for any* REST API.
    """
    base_url = ""

    def __init__(self, api_provider: str, *args, **kwargs):
        self.api_provider_settings: Optional[dict] = settings.API_ENVIRONMENTS.get(api_provider)
        self.is_last_request_valid: Optional[bool] = None  # Not initialized as True so we know if a request was made
        self.response = None

    @property
    def headers(self) -> dict:
        return {}

    def get_complete_url(self, *urls):
        return urljoin(self.base_url, *urls)

    def _validate_response(self, response: Response):
        """
        Override method for more specific validations.
        """
        status_code = response.status_code
        self.is_last_request_valid = True
        if not is_success(status_code):
            self.is_last_request_valid = False
            log.error(f"Unexpected response with status {status_code} occurred. response={response}")
            raise GenericAPIException()

    def make_request(self,
                     method: str,
                     url: str,
                     params: Optional[dict] = None,
                     data: Optional[dict] = None,
                     **request_kwargs) -> Optional[Response]:
        """
        :param method: str, RESTMethods
        :param url: str
        :param params: to send in the query string of the request
        :param data: to send in the body of the request
        :param request_kwargs: other request kwargs
        :return: Response
        """
        try:
            log.info(f"[make_request] Executing request: {url}")
            response = requests.request(method, url, headers=self.headers, params=params, data=data,
                                        **request_kwargs)
            self.response = response
            self._validate_response(response)
        except requests.exceptions.ConnectTimeout as e:
            log.warning("[make_request] Connection timeout")
            log.exception(e)
        except GenericAPIException as e:
            log.warning("[make_request] Error when validating response")
            log.exception(e)
        return self.response


class BaseService(ABC):
    """
    Wrapper around the RestBackend
    """

    _service_output = lambda cls, response: None
    rest_backend = None
    is_valid = None
    output = None

    def process_response(self, response: Response):
        """
        :return: Response/Service Output
        """
        if response is not None and not is_success(response.status_code):
            self.is_valid = False
            return
        self.output = self._service_output(response)
        self.is_valid = True


class BaseOutput:
    data = None

    def __init__(self, response: Response):
        self.response = response
        self.response_data = response.json()
