import datetime
import decimal
import uuid
from typing import Union

from requests import Response

from apis.b2c2_api.b2c2_constants import B2C2BuySellSide, B2C2OrderType
from apis.b2c2_api.b2c2_datastructures import B2C2InstrumentsOutput, B2C2RfqOutput, B2C2OrderOutput, B2C2BalanceOutput, \
    B2C2OpenPositionsOutput
from apis.b2c2_api.b2c2_rest_backend import B2C2RestBackend
from apis.base import BaseService
from apis.constants import RestMethods
from utils.conversion_utils import float_amount_to_str_decimal, datetime_to_string


class B2C2BaseService(BaseService):

    def __init__(self, authorization_token):
        self.rest_backend = B2C2RestBackend(authorization_token)


class B2C2BalanceService(B2C2BaseService):
    _service_output = B2C2BalanceOutput

    @property
    def balance_url(self):
        return self.rest_backend.get_complete_url("balance")

    def get_balance(self) -> Response:
        response = self.rest_backend.make_request(RestMethods.GET, self.balance_url)
        return response


class B2C2OpenPositionsService(B2C2BaseService):
    _service_output = B2C2OpenPositionsOutput

    @property
    def open_positions_url(self):
        return self.rest_backend.get_complete_url("/cfd/open_positions/")

    def get_open_positions(self) -> Response:
        response = self.rest_backend.make_request(RestMethods.GET, self.open_positions_url)
        return response


class B2C2InstrumentsService(B2C2BaseService):
    _service_output = B2C2InstrumentsOutput

    @property
    def instruments_url(self):
        return self.rest_backend.get_complete_url("instruments")

    def get_instruments(self) -> Response:
        response = self.rest_backend.make_request(RestMethods.GET, self.instruments_url)
        return response


class B2C2RfqService(B2C2BaseService):
    _service_output = B2C2RfqOutput

    @property
    def rfq_url(self):
        return self.rest_backend.get_complete_url("request_for_quote/")

    def _get_rfq_post_data(self, instrument_name: str, side: str, quantity: float):
        assert side in B2C2BuySellSide.options(), f"Side not acceptable, must be one of {B2C2BuySellSide.options()}"
        # TODO Ideally represent precision according to the instrument. Default to 2 for every instrument for now.
        quantity = float_amount_to_str_decimal(quantity)
        return {
            'instrument': instrument_name,
            'side': side,
            'quantity': quantity,
            'client_rfq_id': str(uuid.uuid4())
        }

    def post_rfq(self, instrument_name: str, side: str, quantity: float) -> Response:
        response = self.rest_backend.make_request(RestMethods.POST,
                                                  url=self.rfq_url,
                                                  json=self._get_rfq_post_data(instrument_name,
                                                                               side,
                                                                               quantity))
        return response


class B2C2OrderService(B2C2BaseService):
    _service_output = B2C2OrderOutput

    @property
    def order_url(self):
        return self.rest_backend.get_complete_url("order/")

    def _get_order_post_data(self,
                             instrument_name: str,
                             side: str,
                             quantity: float,
                             price: Union[float, decimal.Decimal],
                             order_type: str,
                             valid_until: datetime.datetime,
                             tag: str):
        assert side in B2C2BuySellSide.options(), f"Side not acceptable, must be one of {B2C2BuySellSide.options()}"
        assert order_type in B2C2OrderType.options(), f"Order Type not acceptable, must be one of " \
                                                      f"{B2C2OrderType.options()}"
        valid_until = datetime_to_string(valid_until) if isinstance(valid_until, datetime.datetime) else valid_until
        return {
            'instrument': instrument_name,
            'side': side,
            'quantity': float_amount_to_str_decimal(quantity),
            'client_order_id': str(uuid.uuid4()),
            'price': float_amount_to_str_decimal(price),
            'order_type': order_type,
            'valid_until': valid_until,
            'executing_unit': tag,
        }

    def post_order(self,
                   instrument_name: str,
                   side: str,
                   quantity: Union[float, decimal.Decimal],
                   price: Union[float, decimal.Decimal],
                   order_type: str,
                   valid_until: datetime.datetime,
                   tag: str) -> Response:
        response = self.rest_backend.make_request(RestMethods.POST,
                                                  url=self.order_url,
                                                  json=self._get_order_post_data(instrument_name,
                                                                                 side,
                                                                                 quantity,
                                                                                 price,
                                                                                 order_type,
                                                                                 valid_until,
                                                                                 tag))
        return response
