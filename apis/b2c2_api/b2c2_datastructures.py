from typing import List
from requests import Response
from rest_framework import serializers
from apis.b2c2_api.b2c2_constants import B2C2BuySellSide

from apis.base import BaseOutput
from order_engine.models import B2C2Rfq, B2C2Order, B2C2Trade
from utils.logging_util import log


# ------------ Balance ------------
class B2C2BalanceOutput(BaseOutput):
    def __init__(self, response: Response):
        super().__init__(response)
        self.data = self.response_data


# ------------ Instruments ------------
class B2C2InstrumentSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=32)


class B2C2InstrumentsOutput(BaseOutput):
    def __init__(self, response: Response):
        super().__init__(response)
        log.info(f"Executing Serializer in output. Response Data: {self.response_data}")
        # Technically we can remove the self.instruments and self.response_data to conserve memory
        self.instruments_serializer = B2C2InstrumentSerializer(data=self.response_data, many=True)
        self.instruments_serializer.is_valid(raise_exception=True)
        self.data = self.instruments_serializer.validated_data

    @property
    def spot_instruments(self) -> List[str]:
        return [val["name"] for val in self.instruments_serializer.data if "SPOT" in val["name"]]

    @property
    def cfd_instruments(self) -> List[str]:
        return [val["name"] for val in self.instruments_serializer.data if "CFD" in val["name"]]

    @property
    def all_instruments(self) -> List[str]:
        return [val["name"] for val in self.instruments_serializer.data]


# ------------ RFQ ------------
class B2C2RfqSerializer(serializers.Serializer):
    class Meta:
        model = B2C2Rfq
        fields = '__all__'  # Ideally define these to be more secure but include all for test purposes

    valid_until = serializers.DateTimeField(format="%Y-%m-%dT%H:%M:%S.%fZ")
    rfq_id = serializers.CharField(max_length=64)
    client_rfq_id = serializers.CharField(max_length=64)
    quantity = serializers.DecimalField(max_digits=64, decimal_places=12, coerce_to_string=True)
    side = serializers.ChoiceField(choices=B2C2BuySellSide.options())
    instrument = serializers.CharField(max_length=32)
    price = serializers.DecimalField(max_digits=64, decimal_places=12, coerce_to_string=True)
    created = serializers.DateTimeField(format="%Y-%m-%dT%H:%M:%S.%fZ")


class B2C2RfqOutput(BaseOutput):
    def __init__(self, response: Response):
        super().__init__(response)
        log.info(f"Executing Serializer in output. Response Data: {self.response_data}")
        self.rfq_serializer = B2C2RfqSerializer(data=self.response_data)
        self.rfq_serializer.is_valid(raise_exception=True)
        self.data = self.rfq_serializer.validated_data


# ------------ Order ------------
class B2C2TradeSerializer(serializers.Serializer):
    class Meta:
        model = B2C2Trade
        fields = '__all__'  # Ideally define these to be more secure but include all for test purposes

    instrument = serializers.CharField(max_length=32)
    trade_id = serializers.CharField(max_length=64)
    origin = serializers.CharField(max_length=64, required=False, allow_null=True)
    rfq_id = serializers.CharField(max_length=64, required=False, allow_null=True)
    created = serializers.DateTimeField(format="%Y-%m-%dT%H:%M:%S.%fZ")
    price = serializers.DecimalField(max_digits=64, decimal_places=12, coerce_to_string=True)
    quantity = serializers.DecimalField(max_digits=64, decimal_places=12, coerce_to_string=True)
    order = serializers.CharField(max_length=64)
    side = serializers.ChoiceField(choices=B2C2BuySellSide.options())
    executing_unit = serializers.CharField(max_length=64, allow_null=True, allow_blank=True)


class B2C2OrderSerializer(serializers.Serializer):
    class Meta:
        model = B2C2Order
        fields = '__all__'  # Ideally define these to be more secure but include all for test purposes

    order_id = serializers.CharField(max_length=64)
    client_order_id = serializers.CharField(max_length=64)
    quantity = serializers.DecimalField(max_digits=64, decimal_places=12, coerce_to_string=True)
    side = serializers.ChoiceField(choices=B2C2BuySellSide.options())
    instrument = serializers.CharField(max_length=32)
    price = serializers.DecimalField(max_digits=64, decimal_places=12, coerce_to_string=True)
    executed_price = serializers.DecimalField(max_digits=64, decimal_places=12, coerce_to_string=True,
                                              required=False, allow_null=True)
    executing_unit = serializers.CharField(max_length=64, allow_null=True, allow_blank=True)
    created = serializers.DateTimeField(format="%Y-%m-%dT%H:%M:%S.%fZ")
    trades = B2C2TradeSerializer(required=False, many=True)


class B2C2OrderOutput(BaseOutput):
    def __init__(self, response: Response):
        super().__init__(response)
        log.info(f"Executing Serializer in output. Response Data: {self.response_data}")
        self.order_serializer = B2C2OrderSerializer(data=self.response_data)
        self.order_serializer.is_valid(raise_exception=True)
        self.data = self.order_serializer.validated_data
        self.trades = self.order_serializer.validated_data["trades"]


# ------------ Open Positions ------------
class B2C2OpenPositionsSerializer(serializers.Serializer):
    avg_entry_price = serializers.DecimalField(max_digits=64, decimal_places=12, coerce_to_string=True)
    agg_position = serializers.DecimalField(max_digits=64, decimal_places=12, coerce_to_string=True)
    side = serializers.ChoiceField(choices=B2C2BuySellSide.options())
    instrument = serializers.CharField(max_length=32)


class B2C2OpenPositionsOutput(BaseOutput):
    def __init__(self, response: Response):
        super().__init__(response)
        self.open_positions_serializer = B2C2OpenPositionsSerializer(data=self.response_data)
        self.open_positions_serializer.is_valid(raise_exception=True)
        self.data = self.open_positions_serializer.validated_data
