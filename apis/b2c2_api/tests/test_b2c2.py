import django
import os

os.environ['DJANGO_SETTINGS_MODULE'] = 'b2c2.settings_local'
django.setup()

import datetime
import decimal
import json
from unittest import TestCase
import responses

from apis.b2c2_api.b2c2_constants import B2C2BuySellSide, B2C2OrderType, B2C2Exception, error_code_to_meaning_mapper
from apis.b2c2_api.b2c2_rest_backend import B2C2RestBackend
from apis.b2c2_api.b2c2_services import B2C2InstrumentsService, B2C2RfqService, B2C2OrderService, B2C2BalanceService
from utils.conversion_utils import float_amount_to_str_decimal

DUMMY_AUTH_TOKEN = "abc"


class B2C2RestBackendTests(TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        cls.rest_backend = B2C2RestBackend(DUMMY_AUTH_TOKEN)

    def test_valid_authorization_token(self):
        self.assertIn("Token", self.rest_backend.authorization_token)


class B2C2BalanceServiceTests(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.service = B2C2BalanceService(DUMMY_AUTH_TOKEN)

    @responses.activate
    def test_get_balance(self):
        expected_data = {
            "USD": "0",
            "BTC": "0",
            "JPY": "0",
            "GBP": "0",
            "ETH": "0",
            "EUR": "0",
            "CAD": "0",
            "LTC": "0",
            "XRP": "0",
            "BCH": "0"
        }
        responses.add(responses.GET,
                      self.service.balance_url,
                      json=expected_data, status=200)
        response = self.service.get_balance()
        self.assertEqual(responses.calls[0].request.url, self.service.balance_url)
        self.assertEqual(1, len(responses.calls))
        self.assertEqual(expected_data, response.json())

        self.service.process_response(response)
        self.assertEqual("0", self.service.output.data["USD"])


class B2C2InstrumentsServiceTests(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.service = B2C2InstrumentsService(DUMMY_AUTH_TOKEN)

    @responses.activate
    def test_get_instruments(self):
        expected_data = [{"name": "BTCUSD.CFD"}, {"name": "BTCUSD.SPOT"}]
        responses.add(responses.GET,
                      self.service.instruments_url,
                      json=expected_data, status=200)
        response = self.service.get_instruments()
        self.assertEqual(responses.calls[0].request.url, self.service.instruments_url)
        self.assertEqual(1, len(responses.calls))
        self.assertEqual(expected_data, response.json())

        self.service.process_response(response)
        self.assertEqual(1, len(self.service.output.cfd_instruments))
        self.assertEqual(1, len(self.service.output.spot_instruments))


class B2C2RfqServiceTests(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.service = B2C2RfqService("abc")

    @responses.activate
    def test_get_rfq(self):
        instrument_name = "BTCUSD.SPOT"
        side = B2C2BuySellSide.BUY
        quantity = 12.3431343
        expected_data = {
            "valid_until": "2017-01-01T19:45:22.025464Z",
            "rfq_id": "d4e41399-e7a1-4576-9b46-349420040e1a",
            "client_rfq_id": "149dc3e7-4e30-4e1a-bb9c-9c30bd8f5ec7",
            "quantity": str(quantity),
            "side": side,
            "instrument": instrument_name,
            "price": "700.00000000",
            "created": "2018-02-06T16:07:50.122206Z"
        }
        responses.add(responses.POST,
                      self.service.rfq_url,
                      json=expected_data, status=200)
        response = self.service.post_rfq(instrument_name, side, quantity)
        self.assertEqual(1, len(responses.calls))
        self.assertEqual(responses.calls[0].request.url, self.service.rfq_url)
        request_body = json.loads(responses.calls[0].request.body)
        self.assertEqual(request_body["instrument"], instrument_name)
        self.assertEqual(request_body["side"], side)
        self.assertEqual(request_body["quantity"], float_amount_to_str_decimal(quantity))
        self.assertEqual(expected_data, response.json())

        self.service.process_response(response)
        self.assertIsInstance(self.service.output.data["created"], datetime.datetime)
        self.assertIsInstance(self.service.output.data["price"], decimal.Decimal)

    @responses.activate
    def test_get_rfq_error(self):
        instrument_name = "BTCUSD.SPOT"
        side = B2C2BuySellSide.BUY
        quantity = 12.3431343
        expected_data = {
            "valid_until": "2017-01-01T19:45:22.025464Z",
            "rfq_id": "d4e41399-e7a1-4576-9b46-349420040e1a",
            "client_rfq_id": "149dc3e7-4e30-4e1a-bb9c-9c30bd8f5ec7",
            "quantity": str(quantity),
            "side": side,
            "instrument": instrument_name,
            "price": "700.00000000",
            "created": "2018-02-06T16:07:50.122206Z"
        }
        status_code = 1002
        responses.add(responses.POST,
                      self.service.rfq_url,
                      json=expected_data,
                      status=status_code)  # RFQ does not belong to you

        self.service.post_rfq(instrument_name, side, quantity)
        self.assertEqual(error_code_to_meaning_mapper[status_code], self.service.rest_backend.exception_message)


class B2C2OrderServiceTests(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.service = B2C2OrderService(DUMMY_AUTH_TOKEN)

    @responses.activate
    def test_successful_post_order(self):
        instrument_name = "BTCUSD.SPOT"
        side = B2C2BuySellSide.BUY
        quantity = 12.3431343
        price = "700.00000000"
        tag = "risk-adding-strategy"
        trade_price = "10457.65110000"
        expected_data = {
            "order_id": "d4e41399-e7a1-4576-9b46-349420040e1a",
            "client_order_id": "d4e41399-e7a1-4576-9b46-349420040e1a",
            "quantity": quantity,
            "side": side,
            "instrument": instrument_name,
            "price": price,
            "executed_price": "10457.651100000",
            "executing_unit": tag,
            "trades": [
                {
                    "instrument": instrument_name,
                    "trade_id": "b2c50b72-92d4-499f-b0a3-dee6b37378be",
                    "origin": "rest",
                    "rfq_id": None,
                    "created": "2018-02-26T14:27:53.675962Z",
                    "price": trade_price,
                    "quantity": quantity,
                    "order": "d4e41399-e7a1-4576-9b46-349420040e1a",
                    "side": side,
                    "executing_unit": tag,
                },
                {
                    "instrument": instrument_name,
                    "trade_id": "b2c50b72-92d4-499f-b0a3-dee6b37378be",
                    "origin": "rest",
                    "rfq_id": None,
                    "created": "2018-02-26T14:27:53.675962Z",
                    "price": trade_price,
                    "quantity": quantity,
                    "order": "d4e41399-e7a1-4576-9b46-349420040e1a",
                    "side": side,
                    "executing_unit": tag,
                }
            ],
            "created": "2018-02-06T16:07:50.122206Z"
        }
        responses.add(responses.POST,
                      self.service.order_url,
                      json=expected_data,
                      status=200)

        response = self.service.post_order(instrument_name, side, quantity,
                                           price=float(price),
                                           order_type=B2C2OrderType.FILL_OR_KILL,
                                           valid_until=datetime.datetime.now(),
                                           tag=tag
                                           )
        self.assertEqual(1, len(responses.calls))
        self.assertEqual(responses.calls[0].request.url, self.service.order_url)
        request_body = json.loads(responses.calls[0].request.body)
        self.assertEqual(request_body["instrument"], instrument_name)
        self.assertEqual(request_body["side"], side)
        self.assertEqual(request_body["quantity"], float_amount_to_str_decimal(quantity))
        self.assertEqual(expected_data, response.json())

        self.service.process_response(response)
        self.assertIsInstance(self.service.output.data["created"], datetime.datetime)
        self.assertIsInstance(self.service.output.data["trades"], list)
        self.assertEqual(2, len(self.service.output.trades))
        self.assertEqual(self.service.output.data["trades"][0]["price"], decimal.Decimal(trade_price))

    @responses.activate
    def test_null_post_order(self):
        instrument_name = "BTCUSD.SPOT"
        side = B2C2BuySellSide.BUY
        price = "5.00000000"
        quantity = "1.0000000000"
        tag = "risk-adding-strategy"
        expected_data = {
            "order_id": "d4e41399-e7a1-4576-9b46-349420040e1a",
            "client_order_id": "d4e41399-e7a1-4576-9b46-349420040e1a",
            "quantity": quantity,
            "side": side,
            "instrument": instrument_name,
            "price": price,
            "executed_price": None,
            "trades": [],
            "created": "2018-02-06T16:07:50.122206Z",
            "executing_unit": tag
        }
        responses.add(responses.POST,
                      self.service.order_url,
                      json=expected_data,
                      status=200)

        response = self.service.post_order(instrument_name, side, quantity,
                                           price=float(price),
                                           order_type=B2C2OrderType.FILL_OR_KILL,
                                           valid_until=datetime.datetime.now(),
                                           tag=tag
                                           )
        self.service.process_response(response)
        self.assertEqual(0, len(self.service.output.trades))
