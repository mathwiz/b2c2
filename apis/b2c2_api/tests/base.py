from functools import partial
from unittest.mock import patch

mocked_request = partial(patch, "requests.request")
