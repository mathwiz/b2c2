from typing import Optional

from requests import Response
from rest_framework.status import is_success

from apis.b2c2_api.b2c2_constants import error_code_to_meaning_mapper, B2C2Exception
from apis.base import RestBackend
from apis.constants import ApiProviders
from apis.exceptions import GenericAPIException
from b2c2 import settings
from utils.logging_util import log


class B2C2RestBackend(RestBackend):
    """
    Taken directly from the docs:

    1. Get your account’s balance for all currencies
    2. Get all tradable instruments
    3. Get your account history
    4. Execute trades
    5. Get information about your account (risk exposure…)
    """

    provider = ApiProviders.b2c2
    max_retries = 1  # Intentionally set to 1 since RFQ and related requests are sensitive to change

    def __init__(self, authorization_token: str):
        # Intentional hard failure if provider settings aren't present.
        self.base_url = settings.API_ENVIRONMENTS[self.provider].get('base_url')
        assert authorization_token, "Authorization Token is None"
        self.authorization_token = f"Token {authorization_token}"
        self.exception_message = None
        super().__init__(api_provider=self.provider)

    @property
    def headers(self) -> dict:
        return {
            "Authorization": self.authorization_token,
            'Content-Type': 'application/json',
        }

    def _validate_response(self, response: Response):
        status_code = response.status_code
        self.is_last_request_valid = True
        if not is_success(status_code):
            self.is_last_request_valid = False
            if status_code in error_code_to_meaning_mapper:
                log.error(f"Error from B2C2 {status_code}: "
                          f"{error_code_to_meaning_mapper[status_code]}")
                self.exception_message = error_code_to_meaning_mapper[status_code]
                raise B2C2Exception(error_code_to_meaning_mapper[status_code])
            else:
                log.error(f"Unexpected response with status {status_code} occurred. {response.__dict__}")
                self.exception_message = "Unexpected Error Occurred"
                raise GenericAPIException()

    def make_request(self,
                     method: str,
                     url: str,
                     params: Optional[dict] = None,
                     data: Optional[dict] = None,
                     **request_kwargs) -> Optional[Response]:
        try:
            return super().make_request(method, url, params, data, **request_kwargs)
        except B2C2Exception as e:
            log.warning("[B2C2][make_request] B2C2 Specific Exception")
            log.exception(e)
        return self.response
