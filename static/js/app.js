let rfqModal;
let rfqData;


/*
Generic Error handler function to display each error in bullet points
 */
function handleBackendError(jqXHR, textStatus, errorThrown) {
    if (jqXHR.responseJSON !== null) {
        let errors = jqXHR.responseJSON;
        let errorString = "";
        $.each(errors, function (key) {
            if (key !== "balance_data") {
                errorString += `<li>${key}: ${errors[key][0]}</li>`;
            }
        });
        $("#alert-danger-text")[0].innerHTML = `<ul>${errorString}</ul>`;
        $("#alert-danger-col").css("display", "block");
    }
    updateBalance(jqXHR.responseJSON["balance_data"]);
}

/*
Displays the message after the order is executed. The order can still be rejected.
 */
function handleOrderSuccess(data, textStatus, jqXHR) {
    let successString = "";
    $.each(data, function (key) {
        if (key !== "balance_data") {
            successString += `<li>${key}: ${JSON.stringify(data[key])}</li>`;
        }
    });

    // Check whether the order was accepted or not based on the order_data trades
    // Display message appropriately
    if (data["order_data"].trades == null || data["order_data"].trades.length === 0 || data["order_data"].trades === undefined) {
        $("#alert-danger-text")[0].innerHTML = `
            <p>Order Rejected</p>
            <ul>${successString}</ul>`;
        $("#alert-danger-col").css("display", "block");
    } else {
        $("#alert-success-text")[0].innerHTML = `
            <p>Order Successful</p>
            <ul>${successString}</ul>`;
        $("#alert-success-col").css("display", "block");
    }
    updateBalance(data["balance_data"]);
}

/*
Displays the RFQ result in bullet points and asks the user to confirm.
 */
function handleRfqFormSuccess(data, textStatus, jqXHR) {
    $("#modal-col").css("display", "block");
    let dataString = "";
    $.each(data, function (key) {
        if (key !== "balance_data") {
            dataString += `<li>${key}: ${data[key]}</li>`;
        }
    });
    $("#rfq-modal-body")[0].innerHTML = dataString;
    rfqModal.show();
    rfqData = data;
    updateBalance(data["balance_data"]);
}

function handleOpenPositionsSuccess(data, textStatus, jqXHR) {
    let dataString = "";
    $.each(data, function (key) {
        if (key !== "balance_data") {
            dataString += `<li>${key}: ${data[key]}</li>`;
        }
    });
    $("#alert-success-text")[0].innerHTML = dataString;
    $("#alert-success-col").css("display", "block");
    updateBalance(data["balance_data"]);
}

function updateBalance(balanceData) {
    if (balanceData !== null && balanceData !== undefined) {
        dataString = "";
        $.each(balanceData, function (key) {
            dataString += `<div class='col-6 col-md-3'>
                                <p>${balanceData[key]} <span style='font-weight: bold'>${key}</span></p>
                            </div>`
        });
        $("#balance-data-row")[0].innerHTML = dataString;
    }
}

/*
Generic AJAX request to backend.
 */
function contactBackend(url, data, handleSuccessFn, handleErrorFn = handleBackendError, type = "POST") {
    $.ajax({
        type: type,
        url: url,
        data: data,
        dataType: "json",
        mode: 'same-origin',
        headers: {
            "X-CSRFToken": $("input[name=csrfmiddlewaretoken]")[0].value
        },
        encode: true,
        success: handleSuccessFn,
        error: handleErrorFn,
    });
}


function startTime() {
    let today = new Date();
    let h = today.getHours();
    let m = today.getMinutes();
    let s = today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    $("#digital-time-div")[0].innerHTML = `${h}:${m}:${s}`;
    setTimeout(startTime, 1000);
}

function checkTime(i) {
    if (i < 10) {
        i = "0" + i; // add 0 in front of single digits
    }
    return i;
}

$(document).ready(function () {
    rfqModal = new bootstrap.Modal(document.getElementById('rfq-modal'), {
        keyboard: false
    });

    startTime();

    /*
    RFQ submitted. Contacts backend.
     */
    $("#rfq-form").submit(function (event) {
        event.preventDefault();
        $("#alert-danger-col").css("display", "none");
        $("#alert-success-col").css("display", "none");
        let formData = {
            instrument: $("#id_instruments").val(),
            side: $("#id_buy_sell").val(),
            quantity: parseFloat($("#id_quantity")[0].value).toFixed(4),
            session: $("#id_session").val(),
        };
        contactBackend("api/rfq", formData, handleRfqFormSuccess)
    });

    /*
    Order confirmation. Contacts backend.
     */
    $("#rfq-modal-accept-btn").click(function (event) {
        event.preventDefault();
        rfqModal.hide();
        $("#alert-danger-col").css("display", "none");
        $("#alert-success-col").css("display", "none");
        let formData = {
            rfq_id: rfqData.rfq_id,
            session: $("#id_session").val(),
        };
        contactBackend("api/order", formData, handleOrderSuccess);
    });

    /*
    Get Open Positions. Contacts backend.
     */
    $("#get-open-positions-btn").click(function (event) {
        event.preventDefault();
        $("#alert-danger-col").css("display", "none");
        $("#alert-success-col").css("display", "none");
        contactBackend("api/open_positions", {}, handleOpenPositionsSuccess, handleBackendError, "GET");
    });
});