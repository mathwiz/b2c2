from django.db import models

from apis.b2c2_api.b2c2_constants import B2C2BuySellSide
from trade_account.models import Profile


class B2C2Rfq(models.Model):
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE, related_name="rfq")
    valid_until = models.DateTimeField()
    rfq_id = models.CharField(max_length=64, db_index=True)
    client_rfq_id = models.CharField(max_length=64, db_index=True)
    quantity = models.DecimalField(max_digits=64, decimal_places=12)
    side = models.CharField(choices=B2C2BuySellSide.choices(), max_length=8)
    instrument = models.CharField(max_length=32)
    price = models.DecimalField(max_digits=64, decimal_places=12)
    created = models.DateTimeField()


class B2C2Order(models.Model):
    rfq = models.ForeignKey("B2C2Rfq", on_delete=models.CASCADE, related_name="order", db_index=True)
    order_id = models.CharField(max_length=64, db_index=True)
    client_order_id = models.CharField(max_length=64, db_index=True)
    quantity = models.DecimalField(max_digits=64, decimal_places=12)
    side = models.CharField(choices=B2C2BuySellSide.choices(), max_length=8)
    instrument = models.CharField(max_length=32)
    price = models.DecimalField(max_digits=64, decimal_places=12)
    executed_price = models.DecimalField(max_digits=64, decimal_places=12, null=True)
    executing_unit = models.CharField(max_length=64, null=True, blank=True)
    created = models.DateTimeField()


class B2C2Trade(models.Model):
    order = models.ForeignKey("B2C2Order", on_delete=models.CASCADE, related_name="trades", db_index=True)
    instrument = models.CharField(max_length=32)
    trade_id = models.CharField(max_length=64, db_index=True)
    origin = models.CharField(max_length=64, null=True)
    rfq_id = models.CharField(max_length=64, null=True, db_index=True)
    created = models.DateTimeField()
    price = models.DecimalField(max_digits=64, decimal_places=12)
    quantity = models.DecimalField(max_digits=64, decimal_places=12)
    side = models.CharField(choices=B2C2BuySellSide.choices(), max_length=8)
    executing_unit = models.CharField(max_length=64, null=True, blank=True)
