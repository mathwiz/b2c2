import json

import django
import os

os.environ['DJANGO_SETTINGS_MODULE'] = 'b2c2.settings_local'
django.setup()

import responses
from rest_framework import status

from apis.b2c2_api.b2c2_constants import B2C2BuySellSide
from apis.b2c2_api.b2c2_services import B2C2OrderService, B2C2BalanceService

from unittest import TestCase
from rest_framework.test import APIRequestFactory, force_authenticate
from order_engine.handlers.order_handler import OrderHandler
from order_engine.tests.factories import ProfileFactory, RfqFactory
from order_engine.models import B2C2Order


class OrderAPITests(TestCase):

    def setUp(self) -> None:
        self.factory = APIRequestFactory()

    def execute_request(self, **request_kwargs):
        request = self.factory.post("api/order", **request_kwargs)
        profile = ProfileFactory.create()
        force_authenticate(request, user=profile.user)
        request.user = profile.user
        view = OrderHandler.as_view()
        return view(request)

    def test_bad_request_found(self):
        response = self.execute_request()
        response_data = json.loads(response.content)
        self.assertEqual(status.HTTP_400_BAD_REQUEST, response.status_code)
        self.assertIn("This field is required", response_data["rfq_id"][0])
        self.assertIn("This field is required", response_data["session"][0])

    def test_proper_request_but_rfq_not_found(self):
        response = self.execute_request(data={"session": "xyz", "rfq_id": "123"})
        response_data = json.loads(response.content)
        self.assertEqual(status.HTTP_500_INTERNAL_SERVER_ERROR, response.status_code)
        self.assertIn("Couldn't find RFQ", response_data["Error"][0])

    @responses.activate
    def test_b2c2_api_null_trades(self):
        instrument_name = "BTCUSD.SPOT"
        side = B2C2BuySellSide.BUY
        price = "5.00000000"
        quantity = "1.0000000000"
        tag = "risk-adding-strategy"
        expected_data = {
            "order_id": "d4e41399-e7a1-4576-9b46-349420040e1a",
            "client_order_id": "d4e41399-e7a1-4576-9b46-349420040e1a",
            "quantity": quantity,
            "side": side,
            "instrument": instrument_name,
            "price": price,
            "executed_price": None,
            "trades": [],
            "created": "2018-02-06T16:07:50.122206Z",
            "executing_unit": tag
        }
        dummy_auth_token = "abc"
        service = B2C2OrderService(dummy_auth_token)
        responses.add(responses.POST,
                      service.order_url,
                      json=expected_data,
                      status=200)
        rfq = RfqFactory.create()

        balance_expected_data = {
            "USD": "0",
            "BTC": "0",
            "JPY": "0",
            "GBP": "0",
            "ETH": "0",
            "EUR": "0",
            "CAD": "0",
            "LTC": "0",
            "XRP": "0",
            "BCH": "0"
        }
        balance_service = B2C2BalanceService(dummy_auth_token)
        responses.add(responses.GET,
                      balance_service.balance_url,
                      json=balance_expected_data,
                      status=200)

        response = self.execute_request(data={"session": "xyz", "rfq_id": rfq.rfq_id})
        response_data = json.loads(response.content)
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertIn("balance_data", response_data)
        self.assertIn("order_data", response_data)
        order = B2C2Order.objects.filter(order_id="d4e41399-e7a1-4576-9b46-349420040e1a").first()
        self.assertIsNotNone(order)
