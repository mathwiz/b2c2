import datetime
from decimal import Decimal

import pytz

from apis.b2c2_api.b2c2_constants import B2C2BuySellSide
from order_engine.models import B2C2Rfq
from trade_account.models import Profile

from django.contrib.auth.models import User
import factory.fuzzy


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User

    username = factory.fuzzy.FuzzyText()
    password = factory.fuzzy.FuzzyText()


class ProfileFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Profile

    user = factory.SubFactory(UserFactory)


class RfqFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = B2C2Rfq

    valid_until = datetime.datetime.now(tz=pytz.UTC) + datetime.timedelta(minutes=30)
    rfq_id = factory.fuzzy.FuzzyText()
    client_rfq_id = factory.fuzzy.FuzzyText()
    quantity = Decimal(123.0)
    side = B2C2BuySellSide.BUY
    instrument = "BTCUSD.SPOT"
    price = Decimal(100.0)
    created = datetime.datetime.now(tz=pytz.UTC)
    profile = factory.SubFactory(ProfileFactory)
