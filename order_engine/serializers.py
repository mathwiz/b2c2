from rest_framework import serializers

from apis.b2c2_api.b2c2_constants import B2C2BuySellSide


class RfqSerializer(serializers.Serializer):
    session = serializers.CharField(max_length=64)
    quantity = serializers.DecimalField(max_digits=64, decimal_places=4, coerce_to_string=True)
    side = serializers.ChoiceField(choices=B2C2BuySellSide.options())
    instrument = serializers.CharField(max_length=32)


class OrderSerializer(serializers.Serializer):
    session = serializers.CharField(max_length=64)
    rfq_id = serializers.CharField(max_length=64)
