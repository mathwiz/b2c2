from copy import deepcopy

from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import transaction
from django.http import JsonResponse
from django.urls import reverse_lazy
from rest_framework import status
from rest_framework.views import APIView

from apis.b2c2_api.b2c2_constants import B2C2OrderType, B2C2Exception
from apis.b2c2_api.b2c2_services import B2C2OrderService
from order_engine.handlers.error_handler import ErrorHandler
from order_engine.handlers.rfq_handler import RfqHandler
from order_engine.models import B2C2Rfq, B2C2Order, B2C2Trade
from order_engine.serializers import OrderSerializer
from order_engine.views import get_processed_balance_service
from utils.logging_util import log


class OrderHandler(LoginRequiredMixin, APIView):
    login_url = reverse_lazy("account_login")

    def post(self, request, *args, **kwargs):
        """
        Attempts to:
        1. Get the RFQ from the DB
        2. Post Order
        3. Process Response
        4. Get Balance

        Will return 500/400 error in case of errors with the RAW (for test purposes only) exception messages.
        """
        profile = request.user.profile
        return_data = {}
        serializer = OrderSerializer(data=request.POST)
        if serializer.is_valid():
            # 1. Get the RFQ from the DB
            rfq = self.get_rfq(serializer, return_data)
            if isinstance(rfq, JsonResponse):
                return rfq

            # 2. Attempt to post the order
            order_service = B2C2OrderService(profile.b2c2_auth_token)

            try:
                RfqHandler.verify_rfq_data(rfq.__dict__)
            except B2C2Exception as e:
                log.exception(e)
                return ErrorHandler.json_response(profile.b2c2_auth_token,
                                                  return_data,
                                                  status.HTTP_500_INTERNAL_SERVER_ERROR,
                                                  {"API Error": [e.message]})
            except Exception as e:
                log.exception(e)
                # continue - error from backend

            self.post_order(order_service, rfq)

            # 3. Check response
            if order_service.is_valid:
                self.save_order(order_service, rfq, return_data)
                if return_data.get("Error") or return_data.get("API Error"):
                    return ErrorHandler.json_response(profile.b2c2_auth_token,
                                                      return_data,
                                                      status.HTTP_500_INTERNAL_SERVER_ERROR)

                return_data["order_data"] = order_service.output.data
                status_code = status.HTTP_200_OK
            else:
                return ErrorHandler.json_response(profile.b2c2_auth_token,
                                                  return_data,
                                                  status.HTTP_400_BAD_REQUEST,
                                                  {"Technical Error": [order_service.rest_backend.response.text],
                                                   "API Error": [order_service.rest_backend.exception_message]
                                                   })
        else:
            # Change to exception message
            log.warning(f"Bad request, errors: {serializer.errors}")
            return_data = dict(serializer.errors)
            status_code = status.HTTP_400_BAD_REQUEST

        get_processed_balance_service(profile.b2c2_auth_token, return_data)
        return JsonResponse(return_data, status=status_code, safe=False)

    def get_rfq(self, serializer: OrderSerializer, data: dict):
        """
        Get the previously saved RFQ, prepare a JSONResponse if not found
        """
        rfq: B2C2Rfq = B2C2Rfq.objects.filter(rfq_id=serializer.validated_data["rfq_id"]).first()
        if not rfq:
            data["Error"] = ["Couldn't find RFQ"]
            status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            return JsonResponse(data, status=status_code, safe=False)
        return rfq

    def post_order(self, order_service: B2C2OrderService, rfq: B2C2Rfq):
        """
        Post the order to B2C2
        """
        order_service.process_response(order_service.post_order(
            rfq.instrument,
            rfq.side,
            rfq.quantity,
            rfq.price,
            B2C2OrderType.FILL_OR_KILL,  # TODO Provide option to user instead
            rfq.valid_until,
            ""  # TODO Provide option to user instead
        ))

    def save_order(self, order_service: B2C2OrderService, rfq: B2C2Rfq, data: dict):
        """
        Save Order and Trades
        """
        try:
            with transaction.atomic():
                # These can technically be in the Serializer create function
                order_data = deepcopy(order_service.output.data)
                trades = order_data.pop("trades", [])
                order = B2C2Order.objects.create(**order_data, rfq=rfq)
                for trade in trades:
                    trade.pop("order", None)
                    B2C2Trade.objects.create(**trade, order=order)
        # Catching general exception is not recommended but doing it for test purposes only
        except Exception as e:
            log.exception(e)
            data["Error"] = ["Couldn't save Order and Trades"]
