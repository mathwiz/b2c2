import datetime

from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse
from django.urls import reverse_lazy
from rest_framework import status
from rest_framework.views import APIView

from apis.b2c2_api.b2c2_constants import B2C2RfqValidUntilException, B2C2Exception
from apis.b2c2_api.b2c2_services import B2C2RfqService
from order_engine.handlers.error_handler import ErrorHandler
from order_engine.models import B2C2Rfq
from order_engine.serializers import RfqSerializer
from order_engine.views import get_processed_balance_service
from utils.logging_util import log


class RfqHandler(LoginRequiredMixin, APIView):
    login_url = reverse_lazy("account_login")

    def post(self, request, *args, **kwargs):
        profile = request.user.profile
        return_data = {}
        rfq_serializer = RfqSerializer(data=request.POST)
        if rfq_serializer.is_valid():
            # POST to B2C2
            service = B2C2RfqService(profile.b2c2_auth_token)
            service.process_response(service.post_rfq(rfq_serializer.validated_data["instrument"],
                                                      rfq_serializer.validated_data["side"],
                                                      rfq_serializer.validated_data["quantity"], ))
            if service.is_valid:
                # Save RFQ
                try:
                    B2C2Rfq.objects.create(**service.output.data, profile=profile)
                # Catching general exception is not recommended but doing it for test purposes only
                except Exception as e:
                    log.exception(e)
                    # TODO API Error/Error message can be string instead of list
                    return ErrorHandler.json_response(profile.b2c2_auth_token,
                                                      return_data,
                                                      status.HTTP_500_INTERNAL_SERVER_ERROR,
                                                      {"Error": ["Couldn't save RFQ"]})

                # Validate the RFQ
                try:
                    self.verify_rfq_data(service.output.data)
                except B2C2Exception as e:
                    return_data["API Error"] = [e.message]
                    status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
                else:
                    return_data: dict = service.output.data
                    status_code = status.HTTP_200_OK
            else:
                # TODO API Error/Error message can be string instead of list
                return ErrorHandler.json_response(profile.b2c2_auth_token,
                                                  return_data,
                                                  status.HTTP_400_BAD_REQUEST,
                                                  {"API Error": [service.rest_backend.exception_message],
                                                   "Technical Error": [service.rest_backend.response.text]})
        else:
            # Change to exception message
            log.warning(f"Bad request, errors: {rfq_serializer.errors}")
            return_data = dict(rfq_serializer.errors)
            status_code = status.HTTP_400_BAD_REQUEST

        get_processed_balance_service(profile.b2c2_auth_token, return_data)
        return JsonResponse(return_data, status=status_code, safe=False)

    @staticmethod
    def verify_rfq_data(rfq_data: dict):
        try:
            now = datetime.datetime.now()
            now = now.replace(tzinfo=rfq_data["valid_until"].tzinfo)
            if rfq_data["valid_until"] < now:
                raise B2C2RfqValidUntilException()
        except Exception as e:
            # Only raise an exception if it's B2C2 specific, otherwise continue silently
            if isinstance(e, B2C2RfqValidUntilException):
                raise B2C2RfqValidUntilException from e
