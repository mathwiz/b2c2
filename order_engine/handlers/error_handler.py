from django.http import JsonResponse
from rest_framework import status

from order_engine.views import get_processed_balance_service


class ErrorHandler:
    """
    Initial build of the ErrorHandler. This can be much more sophisticated and cleaner.
    The API between the frontend and backend should also be well defined.

    Left as is for test purposes.
    """

    @staticmethod
    def json_response(auth_token: str,
                      return_data: dict,
                      status_code: int = status.HTTP_500_INTERNAL_SERVER_ERROR,
                      additional_errors: dict = None):
        return_data.update(additional_errors or {})
        get_processed_balance_service(auth_token, return_data)
        return JsonResponse(return_data, status=status_code, safe=False)
