from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse
from django.urls import reverse_lazy
from rest_framework import status
from rest_framework.views import APIView
from apis.b2c2_api.b2c2_services import B2C2BalanceService


class BalanceHandler(LoginRequiredMixin, APIView):
    login_url = reverse_lazy("account_login")

    def get(self, request):
        profile = request.user.profile
        service = B2C2BalanceService(profile.b2c2_auth_token)
        service.process_response(service.get_balance())
        if service.is_valid:
            return JsonResponse(service.output.data)
        else:
            return JsonResponse({"API Error": service.rest_backend.exception_message},
                                status=status.HTTP_400_BAD_REQUEST, safe=False)
