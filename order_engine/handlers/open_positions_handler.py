from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse
from django.urls import reverse_lazy
from rest_framework import status
from rest_framework.views import APIView

from apis.b2c2_api.b2c2_services import B2C2OpenPositionsService
from order_engine.handlers.error_handler import ErrorHandler
from order_engine.views import get_processed_balance_service
from utils.logging_util import log


class OpenPositionsHandler(LoginRequiredMixin, APIView):
    login_url = reverse_lazy("account_login")

    def get(self, request, *args, **kwargs):
        profile = request.user.profile
        open_positions_service = B2C2OpenPositionsService(profile.b2c2_auth_token)
        open_positions_service.process_response(open_positions_service.get_open_positions())
        if open_positions_service.is_valid:
            return_data = open_positions_service.output.data
            get_processed_balance_service(profile.b2c2_auth_token, return_data)
            return JsonResponse(return_data)

        if open_positions_service.output:
            log.warning(f"Bad request, errors: {open_positions_service.output.open_positions_serializer.errors}")

            return ErrorHandler.json_response(profile.b2c2_auth_token,
                                              dict(open_positions_service.output.open_positions_serializer.errors),
                                              status.HTTP_400_BAD_REQUEST)

        return ErrorHandler.json_response(profile.b2c2_auth_token,
                                          {"API Error": [open_positions_service.rest_backend.exception_message],
                                           "Technical Error": [open_positions_service.rest_backend.response.text]},
                                          status.HTTP_500_INTERNAL_SERVER_ERROR)
