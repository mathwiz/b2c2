from django.conf.urls import url

from order_engine.handlers.balance_handler import BalanceHandler
from order_engine.handlers.open_positions_handler import OpenPositionsHandler
from order_engine.handlers.rfq_handler import RfqHandler
from order_engine.handlers.order_handler import OrderHandler

app_name = 'order_engine'

urlpatterns = [
    url(r'^balance$', BalanceHandler.as_view(), name='get_balance'),
    url(r'^rfq$', RfqHandler.as_view(), name='rfq_handler'),
    url(r'^order$', OrderHandler.as_view(), name='order_handler'),
    url(r'^open_positions$', OpenPositionsHandler.as_view(), name='open_positions'),
]
