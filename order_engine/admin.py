from django.contrib import admin

from order_engine.models import B2C2Order, B2C2Rfq, B2C2Trade
from trade_account.models import Profile


class B2C2OrderAdmin(admin.ModelAdmin):
    list_display = [f.name for f in B2C2Order._meta.fields]


class B2C2RfqAdmin(admin.ModelAdmin):
    list_display = [field.name for field in B2C2Rfq._meta.fields]


class B2C2TradeAdmin(admin.ModelAdmin):
    list_display = [field.name for field in B2C2Trade._meta.fields]


admin.site.register(B2C2Order, B2C2OrderAdmin)
admin.site.register(B2C2Rfq, B2C2RfqAdmin)
admin.site.register(B2C2Trade, B2C2TradeAdmin)
