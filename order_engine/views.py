from apis.b2c2_api.b2c2_services import B2C2BalanceService

from utils.logging_util import log


def get_processed_balance_service(authorization_token: str, data: dict) -> B2C2BalanceService:
    # Execute get_balance and return the service
    balance_service = B2C2BalanceService(authorization_token)
    try:
        balance_service.process_response(balance_service.get_balance())
        if balance_service.is_valid:
            data["balance_data"] = balance_service.output.data
    # TODO Catch more specific exceptions. Create an ErrorHandler
    except Exception as e:
        log.exception(e)
        # encompassed in a list to reuse frontend code and be consistent with serializers.errors
        data["API Error"] = [balance_service.rest_backend.exception_message or "General Balance Error"]
    return balance_service
