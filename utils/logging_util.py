__all__ = ['log']

import logging

logging.basicConfig(format='%(asctime)s - %(name)s || %(levelname)s --- %(message)s',
                    datefmt='%m/%d/%Y %I:%M:%S %p',
                    level=logging.INFO)
log = logging.getLogger('b2c2')
