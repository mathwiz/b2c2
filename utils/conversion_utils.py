import datetime
from decimal import Decimal
from typing import Union

from django.utils.datetime_safe import strftime


def float_amount_to_str_decimal(amount: Union[float, Decimal], precision: int = 4) -> str:
    """
    Returns string representation of decimal.
    """
    return str(round(Decimal(amount), precision))


def string_datetime_to_datetime(datetime_str: str) -> datetime.datetime:
    return datetime.datetime.strptime(datetime_str, "%Y-%m-%dT%H:%M:%S.%fZ")


def datetime_to_string(datetime_obj: datetime.datetime, datetime_format="%Y-%m-%dT%H:%M:%S.%fZ") -> datetime.datetime:
    return strftime(datetime_obj, datetime_format)
